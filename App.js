import { StatusBar } from 'expo-status-bar';
import React from 'react';
import { StyleSheet, Text, View } from 'react-native';
import OneSignal from './onesignal/OneSignal';

export default function App() {
  return (
    <View style={styles.container}>
      <OneSignal />
      <Text style={styles.title}>Test OneSignal</Text>
      <Text style={styles.title}>with Expo Bare</Text>
      <StatusBar style="auto" />
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    justifyContent: 'center',
  },
  title: {
    fontSize: 25
  }
});

/* eslint-disable class-methods-use-this */
/* eslint-disable no-console */
import { Component } from 'react';
import { Alert } from 'react-native';
import OneSignal from 'react-native-onesignal'; // Import package from node modules
// import { Actions } from 'react-native-router-flux';
// import { SCREEN } from '../model/Types';
// import User from '../model/UserPlayer';

export default class App extends Component {
  constructor(properties) {
    super(properties);

    console.log('construtor onesignal')

  }

  async componentDidMount() {
    OneSignal.setLogLevel(6, 0);
    OneSignal.setAppId("0bd111fe-6362-4ccf-b07a-7c8824cfbfe6");
    OneSignal.setRequiresUserPrivacyConsent(false);

    /* O N E S I G N A L  H A N D L E R S */
    OneSignal.setNotificationWillShowInForegroundHandler(notifReceivedEvent => {
      this.OSLog("OneSignal: notification will show in foreground:", notifReceivedEvent);
      let notif = notifReceivedEvent.getNotification();

      const button1 = {
        text: "Cancel",
        onPress: () => { notifReceivedEvent.complete(); },
        style: "cancel"
      };

      this.OSLog("notif: ", notif);

      const button2 = { text: "Quero ver", onPress: () => { notifReceivedEvent.complete(notif); this.onOpened(notif); } };

      Alert.alert(notif.title, notif.body, [button1, button2], { cancelable: true });
    });
    OneSignal.setNotificationOpenedHandler(notification => {
      this.OSLog("OneSignal: notification opened:", notification);
      this.onOpened(notification);
    });
    OneSignal.setInAppMessageClickHandler(event => {
      this.OSLog("OneSignal IAM clicked:", event);
    });
    OneSignal.addEmailSubscriptionObserver((event) => {
      this.OSLog("OneSignal: email subscription changed: ", event);
    });
    OneSignal.addSubscriptionObserver(event => {
      this.OSLog("OneSignal: subscription changed:", event);
      this.setState({ isSubscribed: event.to.isSubscribed })
    });
    OneSignal.addPermissionObserver(event => {
      this.OSLog("OneSignal: permission changed:", event);
    });

    const deviceState = await OneSignal.getDeviceState();

    this.setState({
      isSubscribed: deviceState.isSubscribed
    });

  }

  OSLog = (msg, notifReceivedEvent) => {
    console.log('Mensagem: ', msg);
    console.log('Result: ', notifReceivedEvent);
  };

  onReceived(notification) {
    console.log('Notification received: ', notification);
  }

  onOpened(notification) {
    console.warn('Message: ', openResult.notification.body);
    console.warn('Data: ', openResult.notification.additionalData);
    console.warn('isActive: ', openResult.notification.isAppInFocus);
    // console.log('openResult: ', openResult);

    // setTimeout(() => {
    //   if (notification.additionalData) {
    //     if (notification.additionalData.action) {
    //       //console.log(notification.additionalData.action.toLowerCase())
    //       switch (notification.additionalData.action.toLowerCase()) {
    //         case "artigo":
    //           Actions.ArticleDetail({ contentId: parseInt(notification.additionalData.artigoId), fromLink: true });
    //           break;
    //         case "incentivo_dia":
    //           Actions.incentivoDoDia();
    //           break;
    //         case "comunicado_detalhe":
    //           Actions.CommunicationDetail({ id: parseInt(notification.additionalData.comunicadoId) })
    //           break;
    //         case "area_especial":
    //           Actions.SpecialArea({ specialAreaId: notification.additionalData.areaEspecialId });
    //           break;
    //         case "reuniao_palestra":
    //           Actions.core_center({ screen: SCREEN.REUNIAO_PALESTRA });
    //           break;
    //         case "brasil_seikyo":
    //           Actions.BrasilSeikyo({ screen: SCREEN.BS_SEMANA });
    //           break;
    //         case "terceira":
    //           Actions.TerceiraCivilizacao({ screen: SCREEN.TC_LIST });
    //           break;
    //         case "rdez":
    //           Actions.RevistaDez({ screen: SCREEN.RD_LIST });
    //           break;
    //         case "podcast":
    //           Actions.PodcastDetail({ idPodcast: parseInt(notification.additionalData.idPodcast), idEpisodio: parseInt(notification.additionalData.idEpisodio) });
    //           break;
    //         case "video":
    //           Actions.VideoDetail({ programId: parseInt(notification.additionalData.idVideoPrograma), episodeId: parseInt(notification.additionalData.idVideoEpisodio) });
    //           break;
    //         default:
    //           Actions.Home();
    //           break;
    //       }
    //     }
    //   }
    // }, 500);

  }

  onIds(device) {
    console.log('Device info: ', device);
    //User.setPlayerId = device.userId;
  }

  render() {
    return null;
  }
};
